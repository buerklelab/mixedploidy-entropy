#!/bin/sh
module load gcc r 
cd /project/evolgen/entropy_vivaswat/mixedploidy-entropy/

## for diploids
sed -i 's/hexaploid/diploid/g' qsimmisldakest.R

# uncomment the k=2 lines
sed -i '40,42 s/#//' qsimmisldakest.R
# comment the k=3 lines
sed -i '44,46 s/^/#/' qsimmisldakest.R
sed -i 's/g.k3.fst3.txt/g.k2.fst1.txt/' qsimmisldakest.R
sed -i 's/mis4fst3/mis1fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis1fst3/mis2fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis2fst3/mis3fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis3fst3/mis4fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R

# comment the k=2 lines
sed -i '40,42 s/^/#/' qsimmisldakest.R
# uncomment the k=3 lines
sed -i '44,46 s/#//' qsimmisldakest.R
sed -i 's/g.k2.fst3.txt/g.k3.fst1.txt/' qsimmisldakest.R
sed -i 's/mis4fst3/mis1fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis1fst3/mis2fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis2fst3/mis3fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis3fst3/mis4fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R

## for triploids
sed -i 's/diploid/triploid/g' qsimmisldakest.R

# uncomment the k=2 lines
sed -i '40,42 s/#//' qsimmisldakest.R
# comment the k=3 lines
sed -i '44,46 s/^/#/' qsimmisldakest.R
sed -i 's/g.k3.fst3.txt/g.k2.fst1.txt/' qsimmisldakest.R
sed -i 's/mis4fst3/mis1fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis1fst3/mis2fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis2fst3/mis3fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis3fst3/mis4fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R

# comment the k=2 lines
sed -i '40,42 s/^/#/' qsimmisldakest.R
# uncomment the k=3 lines
sed -i '44,46 s/#//' qsimmisldakest.R
sed -i 's/g.k2.fst3.txt/g.k3.fst1.txt/' qsimmisldakest.R
sed -i 's/mis4fst3/mis1fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis1fst3/mis2fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis2fst3/mis3fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis3fst3/mis4fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R

## for tetraploids
sed -i 's/triploid/tetraploid/g' qsimmisldakest.R

# uncomment the k=2 lines
sed -i '40,42 s/#//' qsimmisldakest.R
# comment the k=3 lines
sed -i '44,46 s/^/#/' qsimmisldakest.R
sed -i 's/g.k3.fst3.txt/g.k2.fst1.txt/' qsimmisldakest.R
sed -i 's/mis4fst3/mis1fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis1fst3/mis2fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis2fst3/mis3fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis3fst3/mis4fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R

# comment the k=2 lines
sed -i '40,42 s/^/#/' qsimmisldakest.R
# uncomment the k=3 lines
sed -i '44,46 s/#//' qsimmisldakest.R
sed -i 's/g.k2.fst3.txt/g.k3.fst1.txt/' qsimmisldakest.R
sed -i 's/mis4fst3/mis1fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis1fst3/mis2fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis2fst3/mis3fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis3fst3/mis4fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R

## for hexaploids
sed -i 's/tetraploid/hexaploid/g' qsimmisldakest.R

# uncomment the k=2 lines
sed -i '40,42 s/#//' qsimmisldakest.R
# comment the k=3 lines
sed -i '44,46 s/^/#/' qsimmisldakest.R
sed -i 's/g.k3.fst3.txt/g.k2.fst1.txt/' qsimmisldakest.R
sed -i 's/mis4fst3/mis1fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis1fst3/mis2fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis2fst3/mis3fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis3fst3/mis4fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R

# comment the k=2 lines
sed -i '40,42 s/^/#/' qsimmisldakest.R
# uncomment the k=3 lines
sed -i '44,46 s/#//' qsimmisldakest.R
sed -i 's/g.k2.fst3.txt/g.k3.fst1.txt/' qsimmisldakest.R
sed -i 's/mis4fst3/mis1fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis1fst3/mis2fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis2fst3/mis3fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/mis3fst3/mis4fst1/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst1/fst2/g' qsimmisldakest.R
Rscript qsimmisldakest.R
sed -i 's/fst2/fst3/g' qsimmisldakest.R
Rscript qsimmisldakest.R

