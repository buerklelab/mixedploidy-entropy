### Vivaswat Shastry, Jan 2019
### simulating genotypes for haploids now (sex chromosomes)

set.seed(10041996)

fst<-c(0.05, 0.25)
nind<-100
nloci<-5000
nloci2<-2000
ploidy<-1

### --- simulate ancestral allele frequencies
anc.pi<-rbeta(nloci, 0.8, 0.8)

unlink("anc.pi.txt")
# writing the ancestral allele frequencies into the file 
write(anc.pi, file="anc.pi.txt")

### --- simulate cluster allele frequencies
# k signifies the cluster ID, since we know there are two pops
p.k1.fst1<-data.frame(pop1=numeric(nloci))
p.k2.fst1<-data.frame(pop1=numeric(nloci), pop2=numeric(nloci))
p.k3.fst1<-data.frame(pop1=numeric(nloci), pop2=numeric(nloci), pop3=numeric(nloci))

for(i in 1:nloci){
	p.k1.fst1[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))

	p.k2.fst1[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
	p.k2.fst1[i,2]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))

	p.k3.fst1[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
	p.k3.fst1[i,2]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
	p.k3.fst1[i,3]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[1]), (1-anc.pi[i]) * (-1 + 1/fst[1]))
}

### --- simulate cluster allele frequencies
p.k1.fst2<-data.frame(pop1=numeric(nloci))
p.k2.fst2<-data.frame(pop1=numeric(nloci), pop2=numeric(nloci))
p.k3.fst2<-data.frame(pop1=numeric(nloci), pop2=numeric(nloci), pop3=numeric(nloci))

for(i in 1:nloci){
	p.k1.fst2[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))

	p.k2.fst2[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))
	p.k2.fst2[i,2]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))

	p.k3.fst2[i,1]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))
	p.k3.fst2[i,2]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))
	p.k3.fst2[i,3]<-rbeta(1,anc.pi[i] * (-1 + 1/fst[2]), (1-anc.pi[i]) * (-1 + 1/fst[2]))
}

### --- simulate ancestry combinations (Q matrix lower triangle plus
### diagonal) for sampled individuals, choose from different hybrid
### classes.  Want to model different ancestry classes, not just
### admixture coefficients

### these will parameters for a multinomial draw for locus specific ancestries
Q.k1 <- rep(1, nind)
Q.k2 <- rbind(
			  matrix(c(1,0,0), ncol=3, nrow=0.5*nind, byrow=T),  ## 50 parentals type 1
			  matrix(c(0,0,1), ncol=3, nrow=0.5*nind, byrow=T)  ## 50 parentals type 2
			  )
Q.k3 <- rbind(  ### NB: there are no three way hybrids
			  matrix(c(1,0,0,0,0,0), ncol=6, nrow=33, byrow=T),  ## 30 parentals type 1
			  matrix(c(0,0,1,0,0,0), ncol=6, nrow=33, byrow=T),  ## 30 parentals type 2
			  matrix(c(0,0,0,0,0,1), ncol=6, nrow=34, byrow=T)  ## 30 parentals type 3
			  )

unlink("Q*")
write.table(Q.k1, file="Q.k1.txt", col.names=F, row.names=F)
write.table(Q.k2, file="Q.k2.txt", col.names=F, row.names=F)
write.table(Q.k3, file="Q.k3.txt", col.names=F, row.names=F)

### sample locus specific ancestries for all individuals from a
### multinomial with Q parameters ... translate these into allele
### copies so we can do Bernoulli trials to build up genotypes.  Store
### only genotypes for now

unlink("g*")

## k1
g.k1.fst1<-matrix(0, nrow=nind, ncol=nloci)
for ( locus in 1:nloci ){
	g.k1.fst1[,locus]<-rbinom(nind, ploidy, p.k1.fst1$pop1[locus])
}
todrop<-which(apply(g.k1.fst1,2,sum)==0 | apply(g.k1.fst1,2,sum)==100)
g.k1.fst1<-g.k1.fst1[,-todrop]
g.k1.fst1<-g.k1.fst1[,1:nloci2]
for(locus in 1:nloci2){
	write.table(g.k1.fst1[,locus], file="g.k1.fst1.txt", append=T,
				col.names=F, row.names=F)
}

g.k1.fst2<-matrix(0, nrow=nind, ncol=nloci)
for ( locus in 1:nloci ){
	g.k1.fst2[,locus]<-rbinom(nind, ploidy, p.k1.fst2$pop1[locus])
}
todrop<-which(apply(g.k1.fst2,2,sum)==0 | apply(g.k1.fst2,2,sum)==100)
g.k1.fst2<-g.k1.fst2[,-todrop]
g.k1.fst2<-g.k1.fst2[,1:nloci2]
## printing genotype values into a text file 
for(locus in 1:nloci2){
	#cat(paste("locus", locus, "\n"), file="simfiles/g.k2.fst2.txt", append=T)
	write.table(g.k1.fst2[,locus], file="g.k1.fst2.txt", append=T,
				col.names=F, row.names=F)
}

## k2
z<-numeric(nloci)
g.k2.fst1<-matrix(0, nrow=nind, ncol=nloci)
for (ind in 1:nrow(Q.k2)){
	# basically, z contains a vector that is one-hot
	z<-rmultinom(nloci, 1, Q.k2[ind,])  ## ancestry vector
	for ( locus in 1:nloci ){
		# retreive index of the 1, can be between 1 and 3
		zz<-which(z[,locus] == 1)  ## ancestry for two allele copies in an ind
		if(zz == 1){ # then it is receiving all its alleles from pop1
			g.k2.fst1[ind, locus] <- rbinom(1, ploidy, prob=p.k2.fst1$pop1[locus]) 
		}
		else if(zz==3){ # it has to be entirely from pop2
			g.k2.fst1[ind, locus] <- rbinom(1, ploidy, prob=p.k2.fst1$pop2[locus])
		}
	}
}
todrop<-which(apply(g.k2.fst1,2,sum)==0 | apply(g.k2.fst1,2,sum)==100)
g.k2.fst1<-g.k2.fst1[,-todrop]
g.k2.fst1<-g.k2.fst1[,1:nloci2]
for(locus in 1:nloci2){
	#cat(paste("locus", locus, "\n"), file="simfiles/g.k2.fst2.txt", append=T)
	write.table(g.k2.fst1[,locus], file="g.k2.fst1.txt", append=T,
				col.names=F, row.names=F)
}


g.k2.fst2<-matrix(0, nrow=nind, ncol=nloci)
for (ind in 1:nrow(Q.k2)){
	z<-rmultinom(nloci, 1, Q.k2[ind,])
	for ( locus in 1:nloci ){
		zz<-which(z[,locus] == 1)
		if(zz == 1){
			g.k2.fst2[ind, locus] <- rbinom(1, ploidy, prob=p.k2.fst2$pop1[locus]) 
		}
		else if(zz==3){
			g.k2.fst2[ind, locus] <- rbinom(1, ploidy, prob=p.k2.fst2$pop2[locus]) 
		}
	}
}
todrop<-which(apply(g.k2.fst2,2,sum)==0 | apply(g.k2.fst2,2,sum)==100)
g.k2.fst2<-g.k2.fst2[,-todrop]
g.k2.fst2<-g.k2.fst2[,1:nloci2]
# printing genotype values into a text file 
for(locus in 1:nloci2){
	#cat(paste("locus", locus, "\n"), file="simfiles/g.k2.fst2.txt", append=T)
	write.table(g.k2.fst2[,locus], file="g.k2.fst2.txt", append=T,
				col.names=F, row.names=F)
}

#k3
g.k3.fst1<-matrix(0, nrow=nind, ncol=nloci)
for (ind in 1:nrow(Q.k3)){
	z<-rmultinom(nloci, 1, Q.k3[ind,])  ## ancestry vector
	for ( locus in 1:nloci ){
		zz<-which(z[,locus] == 1)  ## ancestry for two allele copies in an ind
		if(zz == 1){
			g.k3.fst1[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst1$pop1[locus]) 
		}
		else if (zz == 3){
			g.k3.fst1[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst1$pop2[locus]) 
		}
		else if (zz==6){
			g.k3.fst1[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst1$pop3[locus])
		}
	}
}
todrop<-which(apply(g.k3.fst1,2,sum)==0 | apply(g.k3.fst1,2,sum)==100)
g.k3.fst1<-g.k3.fst1[,-todrop]
g.k3.fst1<-g.k3.fst1[,1:nloci2]
for(locus in 1:nloci2){
	write.table(g.k3.fst1[,locus], file="g.k3.fst1.txt", append=T,
				col.names=F, row.names=F)
}

g.k3.fst2<-matrix(0, nrow=nind, ncol=nloci)
for (ind in 1:nrow(Q.k3)){
	z<-rmultinom(nloci, 1, Q.k3[ind,])  ## ancestry vector
	for ( locus in 1:nloci ){
		zz<-which(z[,locus] == 1)  ## ancestry for two allele copies in an ind
		if(zz == 1){
			g.k3.fst2[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst2$pop1[locus]) 
		}
		else if (zz == 3){
			g.k3.fst2[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst2$pop2[locus]) 
		}
		else if(zz==6){
			g.k3.fst2[ind, locus] <- rbinom(1, ploidy, prob=p.k3.fst2$pop3[locus])
		}
	}
}
todrop<-which(apply(g.k3.fst2,2,sum)==0 | apply(g.k3.fst2,2,sum)==100)
g.k3.fst2<-g.k3.fst2[,-todrop]
g.k3.fst2<-g.k3.fst2[,1:nloci2]
# printing genotype values into a text file 
for(locus in 1:nloci2){
	#cat(paste("locus", locus, "\n"), file="simfiles/g.k2.fst2.txt", append=T)
	write.table(g.k3.fst2[,locus], file="g.k3.fst2.txt", append=T,
				col.names=F, row.names=F)
}

