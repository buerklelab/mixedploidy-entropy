nind<-100
nloci<-2000
print("creating matrix")
g<-t(matrix(scan("simfiles/tetraploid/g.k3.fst3.txt",n=nind*nloci),nrow=nind,ncol=nloci))

do.pca<-function(gmat, write.gcov=FALSE, inds=""){
	print("centering genotype matrix")
    gmn<-apply(gmat,1,mean, na.rm=T)
    gmnmat<-matrix(gmn,nrow=nrow(gmat),ncol=ncol(gmat))
    gprime<-gmat-gmnmat ## remove mean
    
	print("finding pairwise covariance")
    gcovarmat<-matrix(NA,nrow=ncol(gmat),ncol=ncol(gmat))
    for(i in 1:ncol(gmat)){
        for(j in i:ncol(gmat)){
            if (i==j){
                gcovarmat[i,j]<-cov(gprime[,i],gprime[,j], use="pairwise.complete.obs")
            }
            else{
                gcovarmat[i,j]<-cov(gprime[,i],gprime[,j], use="pairwise.complete.obs")
                gcovarmat[j,i]<-gcovarmat[i,j]
            }
        }
    }
    if(write.gcov==TRUE){
        inds<-ifelse(inds == "", paste("i", 1:ncol(gmat)), inds)
        write.table(round(gcovarmat,5),file="gcovarmat.txt",
                    quote=F,row.names=F,col.names=inds)
    }
	print("final principal comp analysis")
    prcomp(x=gcovarmat,center=TRUE,scale=FALSE)
}
print("covariate matrix calc")
pcout<-do.pca(g)
pcSummary<-summary(pcout)
print("k-means, lda and writing to file")
library(MASS)
#k2<-kmeans(pcout$x[,1:5],2,iter.max=10,nstart=10,algorithm="Hartigan-Wong")
#ldak2<-lda(x=pcout$x[,1:5], grouping=k2$cluster,CV=TRUE)
#write.table(round(ldak2$posterior,4),quote=F,row.names=F,col.names=F,file="simfiles/diploid/qk3fst4.txt")

k3<-kmeans(pcout$x[,1:5],3,iter.max=10,nstart=10,algorithm="Hartigan-Wong")
ldak3<-lda(x=pcout$x[,1:5], grouping=k3$cluster,CV=TRUE)
write.table(round(ldak3$posterior,5),quote=F,row.names=F,col.names=F,file="simfiles/tetraploid/qk3fst3.txt")

#
#k4<-kmeans(pcout$x[,1:5],4,iter.max=10,nstart=10,algorithm="Hartigan-Wong")
#ldak4<-lda(x=pcout$x[,1:5], grouping=k4$cluster,CV=TRUE)
#write.table(round(ldak4$posterior,5),file="simfiles/diploid/qk4.txt",quote=F,row.names=F,col.names=F)
#
#k5<-kmeans(pcout$x[,1:5],5,iter.max=10,nstart=10,algorithm="Hartigan-Wong")
#ldak5<-lda(x=pcout$x[,1:5], grouping=k5$cluster,CV=TRUE)
#write.table(round(ldak5$posterior,5),file="simfiles/diploid/qk5.txt",quote=F,row.names=F,col.names=F)
#
#k6<-kmeans(pcout$x[,1:5],6,iter.max=10,nstart=10,algorithm="Hartigan-Wong")
#ldak6<-lda(x=pcout$x[,1:5], grouping=k6$cluster,CV=TRUE)
#write.table(round(ldak6$posterior,5),file="simfiles/diploid/qk6.txt",quote=F,row.names=F,col.names=F)
#
#k7<-kmeans(pcout$x[,1:5],7,iter.max=10,nstart=10,algorithm="Hartigan-Wong")
#ldak7<-lda(x=pcout$x[,1:5], grouping=k7$cluster,CV=TRUE)
#write.table(round(ldak7$posterior,5),file="simfiles/diploid/qk7.txt",quote=F,row.names=F,col.names=F)

plot(pcout$x[,'PC1'], pcout$x[,'PC2'], col=c("red", "black", "blue")[k3$cluster], pch=19, cex=0.7,
xlab =  paste("PC1 (", round(pcSummary$importance[2,1]*100, 1), "%)", sep=""),
ylab =  paste("PC2 (", round(pcSummary$importance[2,2]*100, 1), "%)", sep=""), main="PCA (1000 inds x 2000 SNPs )")
points(pcout$x[46:60,'PC1'], pcout$x[46:60,'PC2'], col="black", pch=12)


### -------------------------------------------------------

#inds<-read.csv("indiv_ids.txt", stringsAsFactors=F)

#incommon<-read.table("inds_incommon.txt", stringsAsFactors=F)$V1
#incommon<-paste(incommon, ".GT", sep="")

## working on hdf5 output from entropy
#library(rhdf5)
#qk2 <- h5read("entropy_k2_17june16.hdf5", "q")
#qk2.ch1 <- h5read("entropy_k2_21june16_chain1.hdf5", "q")
#qk2.ch2 <- h5read("entropy_k2_21june16_chain2.hdf5", "q")
#
#q2<-data.frame(inds, q=apply(qk2[,1,], 2, mean))
#
#
#
#
#qest<-apply(rbind( qk2.ch1[,1,], qk2.ch2[,1,]), 2, mean)
#
#write.table(data.frame(inds, q=qest)[inds$ind %in% incommon,], file="qest_50000bp.csv", row.names=F, quote=F, sep=",")
#
#plot(c(qk2.ch1[,2,51], qk2.ch2[,2,51]))
#
#
#mycols<-c("#9E0142","#D53E4F","#F46D43","#FDAE61","#FEE08B","#E6F598","#ABDDA4","#66C2A5","#3288BD","#5E4FA2")
#zprob<-h5read("entropy_k2_21june16_chain1.hdf5", "zprob")
#zprob.image<-t(zprob[1,order(qest),])
#
#image(zprob.image, breaks=seq(0,1,length.out=12), col=mycols)
#dev.print(pdf, "locus_specific_anc_qmodel.pdf")
#
#
#bigQ.ch1<-h5read("entropy_k2_21june16_Q_chain1.hdf5", "Q")
#
#which(qest>0.49 & qest<0.51)
## [1] 117 136 143 144 177 409 432
#plot(c(bigQ.ch1[,2,117], bigQ.ch2[,2,117]))
#plot(c(bigQ.ch1[,2,136], bigQ.ch2[,2,136]))
#
#plot(qest, Qest, ylim=0:1)
#abline(2,-2)
#abline(0,2)
#
#qQch1<-h5read("entropy_k2_21june16_Q_run2ch1.hdf5", "q")
#qQch2<-h5read("entropy_k2_21june16_Q_run2ch2.hdf5", "q")
#dim(qQch1)
## [1] 500   2 472
#qQ<-rbind(qQch1[,1,], qQch2[,1,])
#
#bigQ.ch1<-h5read("entropy_k2_21june16_Q_run2ch1.hdf5", "Q")
#bigQ.ch2<-h5read("entropy_k2_21june16_Q_run2ch2.hdf5", "Q")
#bigQ<-rbind(bigQ.ch1[,2,], bigQ.ch2[,2,])
#
#plot(apply(qQ, 2, mean), apply(bigQ, 2, mean), ylim=0:1,
#     ylab="Q12 (interspecific ancestry)",
#     xlab="q (admixture proportion)")
#abline(2,-2)
#abline(0,2)
#dev.print(pdf, "est.qQ.pdf")
#
#p.ch1<-h5read("entropy_k2_21june16_Q_chain1.hdf5", "p")
#p.est.1<-apply(p.ch1[,1,], 2, mean)
#p.est.2<-apply(p.ch1[,2,], 2, mean)
#
#layout(matrix(c(1,2,3,4), ncol=2, byrow=T), widths=c(5,1), heights=c(5,1))
#par(mar=c(4,4,0.1, 0.1))
#image(zprob.image, breaks=seq(0,1,length.out=11), col=mycols, axes=F,
#      ylab="Individual")
#par(mar=c(4,0.1,0.1, 0.1))
#plot(sort(qest), 1:length(qest), xlab="q", ylab="", type="l", axes=F)
#legend(0.1, 470, legend=paste(seq(0.9,0,-0.1), seq(1,0.1,-0.1), sep="-"), pch=15, col=rev(mycols), bty="n", title="Locus ancestry (z)")
#axis(1, at=c(0,0.5,1), labels=c(0,0.5,1))
#par(mar=c(2,4,0.1,0.1))
#plot(abs(p.est.1-p.est.2), xlab="", ylab="delta", axes=F)
#axis(2, at=c(0,0.5,1), labels=c(0,0.5,1))
#mtext("Locus", side=1, line=1)
#tmp<-hist(abs(p.est.1-p.est.2), plot=F)
#par(mar=c(2, 0.1, 0.1, 0.1))
#plot(tmp$counts, tmp$mids, type="l", ylab="", xlab="", axes=F)
#dev.print(pdf, "locus_specific_anc_Qmodel.pdf")
#
#
#p.ch1.run2<-h5read("entropy_k2_21june16_Q_run2ch1.hdf5", "p")
#deltap.r2<-abs(apply(p.ch1.run2[,1,], 2, mean) - apply(p.ch1.run2[,2,], 2, mean))
#write.table(which(deltap.r2>=0.95), file="anc_informative_focalloci_1000bp.txt",
#            row.names=F, col.names=F, quote=F)
#sum(deltap.r2>=0.95)
##[1] 2205
#sum(deltap.r2>=0.99)
##[1] 775
#write.table(which(deltap.r2>=0.99), file="anc99_informative_focalloci_1000bp.txt",
#            row.names=F, col.names=F, quote=F)
#
#### analysis involving the 775 loci that have delta of >=0.99, but
#### still have missing data
#bigQ.ch1<-h5read("entropy_k2_22june16_Q_run2ch1.hdf5", "Q")
#bigQ.ch2<-h5read("entropy_k2_22june16_Q_run2ch2.hdf5", "Q")
#bigQ<-apply(rbind(bigQ.ch1[,2,], bigQ.ch2[,2,]), 2, mean)
#
#q.ch1<-h5read("entropy_k2_22june16_Q_run2ch1.hdf5", "q")
#q.ch2<-h5read("entropy_k2_22june16_Q_run2ch2.hdf5", "q")
#qest<-apply(rbind(q.ch1[,1,], q.ch2[,1,]), 2, mean)
#
#plot(qest[inds$ind %in% incommon], bigQ[inds$ind %in% incommon],
#     ylim=0:1, pch=19, ylab="interspecific ancestry (Q12)",
#     xlab="admixture proportion (q)")
#points(qest[!(inds$ind %in% incommon)], bigQ[!(inds$ind %in% incommon)])
#segments(0.5,1, 1,0)
#segments(0,0, 0.5,1)
#dev.print(pdf, "est.qQ.pdf")
#
#zprob<-h5read("entropy_k2_22june16_Q_run2ch1.hdf5", "zprob")
#zprob.image<-t(zprob[1,order(qest),])
#
#p.ch1<-h5read("entropy_k2_22june16_Q_run2ch1.hdf5", "p")
#p.est.1<-apply(p.ch1[,1,], 2, mean)
#p.est.2<-apply(p.ch1[,2,], 2, mean)
#
#
#layout(matrix(c(1,2,3,4), ncol=2, byrow=T), widths=c(5,1), heights=c(5,1))
#par(mar=c(4,4,0.1, 0.1))
#image(zprob.image, breaks=seq(0,1,length.out=11), col=mycols, axes=F,
#      ylab="Individual")
#par(mar=c(4,0.1,0.1, 0.1))
#plot(sort(qest), 1:length(qest), xlab="q", ylab="", type="l", axes=F)
#legend(0.1, 470, legend=paste(seq(0.9,0,-0.1), seq(1,0.1,-0.1), sep="-"), pch=15, col=rev(mycols), bty="n", title="Locus ancestry (z)")
#axis(1, at=c(0,0.5,1), labels=c(0,0.5,1))
#par(mar=c(2,4,0.1,0.1))
#plot(abs(p.est.1-p.est.2), xlab="", ylab="delta", axes=F, ylim=0:1)
#axis(2, at=c(0,0.5,1), labels=c(0,0.5,1))
#mtext("Locus", side=1, line=1)
#tmp<-hist(abs(p.est.1-p.est.2), plot=F)
#par(mar=c(2, 0.1, 0.1, 0.1))
#plot(tmp$counts, tmp$mids, type="l", ylab="", xlab="", axes=F)
#dev.print(pdf, "locus_specific_anc_Qmodel_99.pdf")
#
#
##### entropy on two new data sets
#
### 1) populus_luisa_ug_gte5000bp.mpgl
#q.ug.ch1<-h5read("entropy_k2_18aug16_Q_ug_ch1.hdf5", "q")
#q.ug.ch2<-h5read("entropy_k2_18aug16_Q_ug_ch2.hdf5", "q")
#
#qest.ug<-apply(rbind(q.ug.ch1[,1,], q.ug.ch2[,1,]), 2, mean)
#
#write.table(data.frame(inds, q=qest.ug)[inds$ind %in% incommon,],
#            file="qest_ug_gte5000bp.csv", row.names=F, quote=F, sep=",")
#
#
### 2) populus_luisa_hc_newfilter_gte5000bp.mpgl
#q.hc.ch1<-h5read("entropy_k2_18aug16_Q_hc_ch1.hdf5", "q")
#q.hc.ch2<-h5read("entropy_k2_18aug16_Q_hc_ch2.hdf5", "q")
#
#qest.hc<-apply(rbind(q.hc.ch1[,1,], q.hc.ch2[,1,]), 2, mean)
#
#write.table(data.frame(inds, q=qest.hc)[inds$ind %in% incommon,],
#            file="qest_hc_gte5000bp.csv", row.names=F, quote=F, sep=",")
#
#plot(qest.hc, qest.ug)
#
#
#### look at UG or HC
#qQ<-rbind(q.ug.ch1[,1,], q.ug.ch2[,1,])
#
#bigQ.ch1<-h5read("entropy_k2_18aug16_Q_ug_ch1.hdf5", "Q")
#bigQ.ch2<-h5read("entropy_k2_18aug16_Q_ug_ch2.hdf5", "Q")
#bigQ<-rbind(bigQ.ch1[,2,], bigQ.ch2[,2,])
#
#
#zprob<-h5read("entropy_k2_18aug16_Q_hc_ch1.hdf5", "zprob")
#zprob.image<-t(zprob[1,order(qest.ug),])
#
#p.ch1<-h5read("entropy_k2_18aug16_Q_hc_ch1.hdf5", "p")
#p.est.1<-apply(p.ch1[,1,], 2, mean)
#p.est.2<-apply(p.ch1[,2,], 2, mean)
#
#layout(matrix(c(1,2,3,4), ncol=2, byrow=T), widths=c(5,1), heights=c(5,1))
#par(mar=c(4,4,0.1, 0.1))
#image(zprob.image, breaks=seq(0,1,length.out=11), col=mycols, axes=F,
#      ylab="Individual")
#par(mar=c(4,0.1,0.1, 0.1))
#plot(sort(qest.ug), 1:length(qest.ug), xlab="q", ylab="", type="l", axes=F)
#legend(0.1, 470, legend=paste(seq(0.9,0,-0.1), seq(1,0.1,-0.1), sep="-"), pch=15, col=rev(mycols), bty="n", title="Locus ancestry (z)")
#axis(1, at=c(0,0.5,1), labels=c(0,0.5,1))
#par(mar=c(2,4,0.1,0.1))
#plot(abs(p.est.1-p.est.2), xlab="", ylab="delta", axes=F)
#axis(2, at=c(0,0.5,1), labels=c(0,0.5,1))
#mtext("Locus", side=1, line=1)
#tmp<-hist(abs(p.est.1-p.est.2), plot=F)
#par(mar=c(2, 0.1, 0.1, 0.1))
#plot(tmp$counts, tmp$mids, type="l", ylab="", xlab="", axes=F)
#dev.print(pdf, "locus_specific_anc_Qmodel_ug.pdf", height=9, width=9)
#
#
### both new data sets have lowere Q estimates than previously
#library(rhdf5)
#par(mfrow=c(2,2))
#plot(apply(h5read("entropy_k2_21june16_Q_run2ch1.hdf5", "Q")[,2,], 2, mean),
#     apply(h5read("entropy_k2_18aug16_Q_hc_ch1.hdf5", "Q")[,2,], 2, mean),
#     xlim=0:1, ylim=0:1, xlab="21june16 Q estimate", ylab="18aug16 Q estimate")
#abline(0,1)
#plot(apply(h5read("entropy_k2_22june16_Q_run2ch1.hdf5", "Q")[,2,], 2, mean),
#     apply(h5read("entropy_k2_18aug16_Q_hc_ch1.hdf5", "Q")[,2,], 2, mean),
#     xlim=0:1, ylim=0:1, xlab="22june16 Q est (775 loci with delta >0.99)", ylab="18aug16 Q estimate")
#abline(0,1)
#plot(apply(h5read("entropy_k2_22june16_Q_run2ch1.hdf5", "Q")[,2,], 2, mean),
#     apply(h5read("entropy_k2_21june16_Q_run2ch1.hdf5", "Q")[,2,], 2, mean),
#     xlim=0:1, ylim=0:1, xlab="22june16 Q est (775 loci with delta >0.99)",
#     ylab="21jun16 Q estimate")
#abline(0,1)
#plot(apply(h5read("entropy_k2_21june16_Q_run2ch1.hdf5", "q")[,1,], 2, mean),
#     apply(h5read("entropy_k2_18aug16_Q_hc_ch1.hdf5", "q")[,1,], 2, mean),
#     xlim=0:1, ylim=0:1, xlab="21june16 q est", ylab="18aug16 q est")
#abline(0,1)
#dev.print(pdf, "qQ18aug16.pdf")
