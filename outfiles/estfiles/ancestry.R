## R script to plot true vs inferred q
## Vivaswat Shastry -- Mar 1919

# picking tetraploids with Fst=0.1, coverage=1x,4x
library(RColorBrewer)
library(rhdf5)

mycols<-brewer.pal(5,name="Greens")[c(2,3,4)]

w.q1<-h5read("../tetraploid/outgl3.fst2.1x.hdf5","q")
q1<-apply(w.q1,c(2,3),mean)
qt1<-matrix(scan("qk3true3a.txt"),nrow=100,ncol=3,byrow=T)

pdf("../trueqvsest.pdf",height=5,width=5.5)
boxplot(t(q1)~qt1, col=mycols[1], border=mycols[1], yaxt="n", xlab="True admixture q", ylab="Estimated admixture q", frame=F, outpch=19, outcol=mycols[1])
axis(2, at=seq(0,1,0.25), labels=c("0","0.25","0.5","0.75","1"))
abline(v=seq(1,5,1),h=seq(0,1,0.25),lty=2,col="gray")
par(new=T)
#boxplot(t(q1), col=mycols[1], border=mycols[1], yaxt="n", xlab="True admixture q", ylab="Estimated admixture q", frame=F, outpch=19, outcol=mycols[1],xaxt="n")

w.q2<-h5read("../tetraploid/outgl3.fst2.2x.hdf5","q")
q2<-apply(w.q2,c(2,3),mean)
qt2<-matrix(scan("qk3true3a.txt"),nrow=100,ncol=3,byrow=T)

w.q3<-h5read("../tetraploid/outgl3.fst2.4x.hdf5","q")
q3<-apply(w.q3,c(2,3),mean)

boxplot(t(q2)~qt2, col=mycols[2], border=mycols[2], add=T, axes=F, outpch=19, outcol=mycols[2]) 
boxplot(t(q3)~qt2, col=mycols[3], border=mycols[3], add=T, axes=F, outpch=19, outcol=mycols[2]) 
legend(title="Sequence depth",fill=mycols,legend=c("1x","2x","4x"),"bottomright",cex=0.9)
#dev.off()
