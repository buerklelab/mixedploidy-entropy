# entropy


## Contacts

Please contact Vivaswat Shastry (vshastry@uwyo.edu) or Alex Buerkle (buerkle@uwyo.edu)
with questions.

## User manual and manuscript

The user manual for `entropy` and an example workflow is located in [vignette_entropy.pdf](vignette_entropy.pdf).

The methods are described in **Shastry, V., Adams, P.E., Lindtke, D., Mandeville, E.G., Parchman, T.L., Gompert, Z. and Buerkle, C.A. 2021. Model-based genotype and ancestry estimation for potential hybrids with mixed-ploidy.** [Molecular Ecology Resources 21: 1434-1451](https://doi.org/10.1111/1755-0998.13330) (and the associated [preprint](https://www.biorxiv.org/content/10.1101/2020.07.31.231514v2)).

## Installation

See [Install.md](Install.md).

## Change log

Version 2.0 - December 2018

Version 1.2 - August 2013 (specifications described in Gompert et. al. 2014)
